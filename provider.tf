terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.27.0"
    }
    null = {
      source = "hashicorp/null"
      version = "3.1.1"
    }
  }
}

provider "google" {
      #path for GCP service account credentials
   credentials = "${file("keyfile.json")}"
      # GCP project ID
   project     = "jeffin-eldhose-project"
      # Any region of your choice
   region      = "us-central1"
      # Any zone of your choice      
   zone        = "us-central1-a"
}
